package com.example.mvvm.viewModal

import android.view.View
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.example.mvvm.model.User
import java.util.*
import com.example.mvvm.BR

class UserViewModal(private val user:User):Observer, BaseObservable() {

    init {
        user.addObserver(this)
    }

    override fun update(p0: Observable?, p1: Any?) {
        if (p1 is String) {
            if (p1 == "age") {
                notifyPropertyChanged(BR.age)
            } else if (p1 == "firstName" || p1 == "lastName") {
                notifyPropertyChanged(BR.name)
            } else if (p1 == "imageUrl") {
                notifyPropertyChanged(BR.imageUrl)
            } else if (p1 == "tagline") {
                notifyPropertyChanged(BR.tagline)
            } else if (p1 == "female") {
                notifyPropertyChanged(BR.gender)
            }
        }
    }

    val name: String
        @Bindable get() = user.firstName + " " + user.lastName

    val age: String
        @Bindable get() = if (user.age <= 0) "" else String.format(Locale.ENGLISH, "%d years old", user.age)

    val gender:String
        @Bindable get() = if(user.female) "Female" else "Male"

    val imageUrl:String
        @Bindable get() = user.imageUrl

    val tagline:String
        @Bindable get() = "Tagline: " + user.tagline

    fun onButtonClick(view:View){
        this.user.age = 35
        this.user.imageUrl = "https://media.giphy.com/media/w7M8g9cTom0Du/giphy.gif"
        this.user.tagline = "Now he has grown up..."
    }
}